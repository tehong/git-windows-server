## Git for microsoft/windowsservercore docker container

Added Chocolatey, git and posh-git modules from microsoft/windowsservercore docker container.  
(**You can only run this under Docker for Windows only**)

### Feature

- microsoft/windowsservercore:latest
- Chocolatey:latest
- chocolatey's git:latest
- NuGet
- posh-git

### Dockerfile

```shell
# base container  
FROM microsoft/windowsservercore:latest    
MAINTAINER ehong
#ENV chocolateyVersion 0.10.3
ENV ChocolateyUseWindowsCompression false
# Set your PowerShell execution policy
RUN powershell Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Force
# Install Chocolatey
RUN powershell -NoProfile -ExecutionPolicy Bypass -Command "iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))" && SET "PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin"
# Install Chocolatey packages
RUN choco install git.install -y 
# Install PowerShell modules
RUN powershell Install-PackageProvider NuGet -MinimumVersion '2.8.5.201' -Force
RUN powershell Set-PSRepository -Name PSGallery -InstallationPolicy Trusted
RUN powershell Install-Module -Name 'posh-git'
```